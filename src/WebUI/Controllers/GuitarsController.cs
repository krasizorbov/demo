﻿using System.Threading.Tasks;
using demo.Application.Common.Models;
using demo.Application.Guitar.Commands.CreateGuitar;
using demo.Application.Guitars.Commands.DeleteGuitar;
using demo.Application.Guitars.Commands.UpdateGuitar;
using demo.Application.Guitars.Queries.GetGuitarById;
using demo.Application.Guitars.Queries.GetGuitarsWithPagination;
using Microsoft.AspNetCore.Mvc;
using demo.Application.Guitars.Dto;
using System.ComponentModel;
using System.Collections.Generic;

namespace demo.WebUI.Controllers
{
    [DisplayName("Guitars")]
    public class GuitarsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<List<GetAllGuitarsDto>>> GetGuitarsWithPagination([FromQuery] GetGuitarWithPaginationQuery query)
        {
            return await Mediator.Send(query);
        }

        [HttpGet]
        public async Task<ActionResult<List<GetGuitarsDto>>> GetGuitarsGroupedByColor([FromQuery] GetGuitarsGroupedByColorQuery query)
        {
            return await Mediator.Send(query);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetGuitarByIdDto>> GetGuitarById(int id)
        {
            var guitar = await Mediator.Send(new GetGuitarByIdQuery(id));

            return guitar == null ? NotFound() : Ok(guitar);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateGuitarCommand command)
        {
            if (id != command.Id) return BadRequest();

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateGuitarCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGuitarCommand(id));

            return NoContent();
        }
    }
}
