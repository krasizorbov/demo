﻿using System.Collections.Generic;
using System.Threading.Tasks;
using demo.Application.Guitarists.Commands.CreateGuitarist;
using demo.Application.Guitarists.Commands.DeleteGuitarist;
using demo.Application.Guitarists.Commands.UpdateGuitarist;
using demo.Application.Guitarists.Queries.GetGuitaristById;
using demo.Application.Guitarists.Queries.GetGuitaristsWithPagination;
using demo.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace demo.WebUI.Controllers
{
    public class GuitaristsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<List<GetGuitaristsDto>>> Get()
        {
            return await Mediator.Send(new GetGuitaristsWithPaginationQuery());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Guitarist>> GetGuitaristById(int id)
        {
            var guitarist = await Mediator.Send(new GetGuitaristByIdQuery(id));

            return guitarist == null ? NotFound() : Ok(guitarist);
        }


        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateGuitaristCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateGuitaristCommand command)
        {
            if (id != command.Id) return BadRequest();

            await Mediator.Send(command); 

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGuitaristCommand { Id = id });

            return NoContent();
        }
    }
}
