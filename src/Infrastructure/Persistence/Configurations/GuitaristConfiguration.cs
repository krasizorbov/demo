﻿using demo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace demo.Infrastructure.Persistence.Configurations
{
    public class GuitaristConfiguration : IEntityTypeConfiguration<Guitarist>
    {
        public void Configure(EntityTypeBuilder<Guitarist> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.GuitaristName)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.BandName)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.Genre)
                .HasMaxLength(20)
                .IsRequired();

            builder.HasMany(t => t.Guitars)
                .WithOne(tr => tr.Guitarist)
                .HasForeignKey(tr => tr.GuitaristId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
