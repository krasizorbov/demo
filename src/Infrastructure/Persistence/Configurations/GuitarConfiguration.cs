﻿using demo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace demo.Infrastructure.Persistence.Configurations
{
    public class GuitarConfiguration : IEntityTypeConfiguration<Guitar>
    {
        public void Configure(EntityTypeBuilder<Guitar> builder)
        { 

            builder.HasKey(t => t.Id);

            builder.Property(t => t.Make)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.Type)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.Name)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.Color)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(t => t.Description)
                .HasMaxLength(500)
                .IsRequired();

            builder.HasOne(t => t.Guitarist)
                .WithMany(g => g.Guitars)
                .HasForeignKey(t => t.GuitaristId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();


        }
    }
}
