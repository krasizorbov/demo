﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace demo.Infrastructure.Persistence.Migrations
{
    public partial class GuitaristTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GuitaristId",
                table: "Guitars",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Guitarist",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GuitaristName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    BandName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guitarist", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Guitars_GuitaristId",
                table: "Guitars",
                column: "GuitaristId");

            migrationBuilder.AddForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars",
                column: "GuitaristId",
                principalTable: "Guitarist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars");

            migrationBuilder.DropTable(
                name: "Guitarist");

            migrationBuilder.DropIndex(
                name: "IX_Guitars_GuitaristId",
                table: "Guitars");

            migrationBuilder.DropColumn(
                name: "GuitaristId",
                table: "Guitars");
        }
    }
}
