﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace demo.Infrastructure.Persistence.Migrations
{
    public partial class GuitaristTable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars");

            migrationBuilder.AddForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars",
                column: "GuitaristId",
                principalTable: "Guitarist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars");

            migrationBuilder.AddForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars",
                column: "GuitaristId",
                principalTable: "Guitarist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
