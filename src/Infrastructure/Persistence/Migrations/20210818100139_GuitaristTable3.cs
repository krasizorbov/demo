﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace demo.Infrastructure.Persistence.Migrations
{
    public partial class GuitaristTable3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Guitarist",
                table: "Guitarist");

            migrationBuilder.RenameTable(
                name: "Guitarist",
                newName: "Guitarists");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Guitarists",
                table: "Guitarists",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Guitars_Guitarists_GuitaristId",
                table: "Guitars",
                column: "GuitaristId",
                principalTable: "Guitarists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Guitars_Guitarists_GuitaristId",
                table: "Guitars");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Guitarists",
                table: "Guitarists");

            migrationBuilder.RenameTable(
                name: "Guitarists",
                newName: "Guitarist");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Guitarist",
                table: "Guitarist",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Guitars_Guitarist_GuitaristId",
                table: "Guitars",
                column: "GuitaristId",
                principalTable: "Guitarist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
