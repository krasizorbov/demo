﻿using demo.Application.Common.Interfaces;
using System;

namespace demo.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
