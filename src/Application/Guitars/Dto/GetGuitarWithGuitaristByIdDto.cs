﻿

namespace demo.Application.Guitars.Dto
{
    using demo.Application.Common.Mappings;
    using Domain.Entities;

    public class GetGuitarWithGuitaristByIdDto : IMapFrom<Guitarist>
    {
        public int Id { get; set; }

        public string Make { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public Guitarist Guitarist { get; set; }

    }
}
