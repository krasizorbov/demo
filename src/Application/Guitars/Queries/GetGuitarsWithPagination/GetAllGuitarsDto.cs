﻿namespace demo.Application.Guitars.Queries.GetGuitarsWithPagination
{
    using demo.Application.Common.Mappings;
    using demo.Domain.Entities;

    public class GetAllGuitarsDto : IMapFrom<Guitar>
    {
        public int Id { get; set; }

        public string Make { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public int GuitaristId { get; set; }

    }
}

