﻿
namespace demo.Application.Guitars.Queries.GetGuitarsWithPagination
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using demo.Application.Common.Interfaces;
    using demo.Domain.Entities;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetGuitarWithPaginationQuery : IRequest<List<GetAllGuitarsDto>>
    {
        public int PageNumber { get; set; } = 0;
        public int? PageSize { get; set; } = 10;
        public int SkipPageCount => PageNumber * PageSize.GetValueOrDefault();
    }

    public class GetGuitarWithPaginationQueryHandler : IRequestHandler<GetGuitarWithPaginationQuery, List<GetAllGuitarsDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetGuitarWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<GetAllGuitarsDto>> Handle(GetGuitarWithPaginationQuery request, CancellationToken cancellationToken)
        {
            var guitars = _context.Guitars.AsQueryable<Guitar>();

            if (request.SkipPageCount >= 0)
            {
                guitars = guitars.Skip(request.SkipPageCount).Take(request.PageSize.GetValueOrDefault());
            }

            return await guitars
                .AsNoTracking()
                .ProjectTo<GetAllGuitarsDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
