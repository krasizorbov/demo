﻿namespace demo.Application.Guitars.Queries.GetGuitarsWithPagination
{
    public class GetGuitarsDto
    {
        public string Color { get; set; }
        public int Count { get; set; }
    }
}
