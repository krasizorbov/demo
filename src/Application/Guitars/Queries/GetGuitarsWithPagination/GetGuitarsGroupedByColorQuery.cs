﻿namespace demo.Application.Guitars.Queries.GetGuitarsWithPagination
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using demo.Application.Common.Interfaces;
    using demo.Domain.Entities;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetGuitarsGroupedByColorQuery : IRequest<List<GetGuitarsDto>>
    {
        
        public int PageNumber { get; set; } = 0;
        public int? PageSize { get; set; } = 10;
        public int SkipPageCount => PageNumber * PageSize.GetValueOrDefault();
    }

    public class GetGuitarsGroupedByColorQueryHandler : IRequestHandler<GetGuitarsGroupedByColorQuery, List<GetGuitarsDto>>
    {
        private readonly IApplicationDbContext _context;

        public GetGuitarsGroupedByColorQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<GetGuitarsDto>> Handle(GetGuitarsGroupedByColorQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Guitar> guitars = _context.Guitars.AsQueryable<Guitar>();

            if (request.SkipPageCount >= 0)
            {
                guitars = guitars.Skip(request.SkipPageCount).Take(request.PageSize.GetValueOrDefault());
            }

            return await guitars
                .GroupBy(g => g.Color)
                .Select(p => new GetGuitarsDto
                {
                    Color = p.Key,
                    Count = p.Count()
                })
                .OrderByDescending(x => x.Color)
                .ToListAsync(cancellationToken);
        }
    }
}
