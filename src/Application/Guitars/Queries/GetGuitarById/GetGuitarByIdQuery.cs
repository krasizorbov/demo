﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using demo.Application.Common.Interfaces;
using demo.Application.Guitars.Dto;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace demo.Application.Guitars.Queries.GetGuitarById
{

    public record GetGuitarByIdQuery(int Id) : IRequest<GetGuitarByIdDto>;

    public class GetGuitarByIdQueryHandler : IRequestHandler<GetGuitarByIdQuery, GetGuitarByIdDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetGuitarByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetGuitarByIdDto> Handle(GetGuitarByIdQuery request, CancellationToken cancellationToken)
        {
            return await _context.Guitars
                .ProjectTo<GetGuitarByIdDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == request.Id);
        }
    }

}
