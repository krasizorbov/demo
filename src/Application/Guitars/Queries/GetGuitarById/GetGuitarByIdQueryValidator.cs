﻿using FluentValidation;

namespace demo.Application.Guitars.Queries.GetGuitarById
{
    public class GetGuitarByIdQueryValidator :AbstractValidator<GetGuitarByIdQuery>
    {
        public GetGuitarByIdQueryValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithMessage("Id is required.");
        }
    }
}
