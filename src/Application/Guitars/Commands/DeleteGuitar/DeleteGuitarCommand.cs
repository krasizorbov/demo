﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Exceptions;
using demo.Application.Common.Interfaces;
using MediatR;

namespace demo.Application.Guitars.Commands.DeleteGuitar
{
    public record DeleteGuitarCommand(int Id) : IRequest;

    public class DeleteGuitarCommandHandler : IRequestHandler<DeleteGuitarCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteGuitarCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteGuitarCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Guitars.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Guitar), request.Id);
            }

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
