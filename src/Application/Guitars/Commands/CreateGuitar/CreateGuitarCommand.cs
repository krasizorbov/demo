﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Interfaces;
using MediatR;

namespace demo.Application.Guitar.Commands.CreateGuitar
{
    public class CreateGuitarCommand : IRequest<int>
    {
        public string Make { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public int GuitaristId { get; set; }

    }

    public class CreateGuitarCommandHandler : IRequestHandler<CreateGuitarCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateGuitarCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateGuitarCommand request, CancellationToken cancellationToken)
        {
            var entity = new Domain.Entities.Guitar
            {
                Make = request.Make,
                Type = request.Type,
                Name = request.Name,
                Color = request.Color,
                Description = request.Description,
                GuitaristId= request.GuitaristId
            };

            _context.Guitars.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
