﻿using demo.Application.Guitar.Commands.CreateGuitar;
using FluentValidation;

namespace demo.Application.Guitars.Commands.CreateGuitar
{
    public class CreateGuitarCommandValidator : AbstractValidator<CreateGuitarCommand>
    {
        public CreateGuitarCommandValidator()
        {

            RuleFor(v => v.Make)
                .NotEmpty().WithMessage("Make is required.")
                .MaximumLength(20).WithMessage("Make must not exceed 20 characters.");

            RuleFor(v => v.Type)
                .NotEmpty().WithMessage("Type is required.")
                .MaximumLength(20).WithMessage("Type must not exceed 20 characters.");

            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(20).WithMessage("Name must not exceed 20 characters.");

            RuleFor(v => v.Color)
                .NotEmpty().WithMessage("Color is required.")
                .MaximumLength(20).WithMessage("Color must not exceed 20 characters.");

            RuleFor(v => v.Description)
                .NotEmpty().WithMessage("Description is required.")
                .MaximumLength(255).WithMessage("Description must not exceed 255 characters.");
        }
    }
}
