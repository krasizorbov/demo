﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Exceptions;
using demo.Application.Common.Interfaces;
using MediatR;

namespace demo.Application.Guitars.Commands.UpdateGuitar
{
    public record UpdateGuitarCommand(int Id, string Make, string Type, string Name, string Color, string Description, int GuitaristId) : IRequest;

    public class UpdateGuitarCommandHandler : IRequestHandler<UpdateGuitarCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateGuitarCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateGuitarCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Guitars.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Guitar), request.Id);
            }

            entity.Make = request.Make;
            entity.Type = request.Type;
            entity.Name = request.Name;
            entity.Color = request.Color;
            entity.Description = request.Description;
            entity.GuitaristId = request.GuitaristId;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
