﻿using demo.Application.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace demo.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
