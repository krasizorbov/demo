﻿using demo.Domain.Common;
using System.Threading.Tasks;

namespace demo.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
