﻿using demo.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace demo.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<TodoList> TodoLists { get; set; }

        DbSet<TodoItem> TodoItems { get; set; }

        DbSet<Domain.Entities.Guitar> Guitars { get; set; }

        DbSet<Guitarist> Guitarists { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
