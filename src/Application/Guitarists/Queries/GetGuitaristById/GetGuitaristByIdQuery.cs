﻿namespace demo.Application.Guitarists.Queries.GetGuitaristById
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using demo.Application.Common.Interfaces;
    using demo.Application.Guitarists.Dto;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public record GetGuitaristByIdQuery(int Id) : IRequest<GuitaristListDto>;

    public class GetGuitarByIdQueryHandler : IRequestHandler<GetGuitaristByIdQuery, GuitaristListDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetGuitarByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GuitaristListDto> Handle(GetGuitaristByIdQuery request, CancellationToken cancellationToken)
        {

            //return await _context.Guitarists.Where(x => x.Id == request.Id).Select(x => new GuitarListDto
            //{
            //    Id = x.Id,
            //    GuitaristName = x.GuitaristName,
            //    BandName = x.BandName,
            //    Genre = x.Genre,
            //    Guitars = _context.Guitars.Where(y => y.Color == "Black" && y.Guitarist.Id == x.Id).Select(g => new GetGuitarByIdDto
            //    {
            //        Id = g.Id,
            //        Make = g.Make,
            //        Type = g.Type,
            //        Name = g.Name,
            //        Color = g.Color,
            //        Description = g.Description
            //    })
            //    .ToList()
            //}).FirstOrDefaultAsync();

            //return await _context.Guitars.Where(y => y.Color == "Black" && y.Guitarist.Id == request.Id).Select(g => new GetGuitarWithGuitaristByIdDto
            //{
            //    Id = g.Id,
            //    Make = g.Make,
            //    Type = g.Type,
            //    Name = g.Name,
            //    Color = g.Color,
            //    Description = g.Description,
            //    Guitarist = g.Guitarist
            //}).ToListAsync();

            return await _context.Guitarists
            .AsNoTracking()
            .ProjectTo<GuitaristListDto>(_mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}