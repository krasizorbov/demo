﻿using FluentValidation;

namespace demo.Application.Guitarists.Queries.GetGuitaristById
{
    public class GetGuitaristByIdQueryValidator : AbstractValidator<GetGuitaristByIdQuery>
    {
        public GetGuitaristByIdQueryValidator()
        {
            RuleFor(x => x.Id).NotEmpty().WithMessage("Id is required.");
        }
    }
}
