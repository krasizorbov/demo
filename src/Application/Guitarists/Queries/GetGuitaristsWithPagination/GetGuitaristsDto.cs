﻿namespace demo.Application.Guitarists.Queries.GetGuitaristsWithPagination
{
    using System.Collections.Generic;
    using demo.Application.Common.Mappings;
    using demo.Domain.Entities;

    public class GetGuitaristsDto : IMapFrom<Guitarist>
    {
        public GetGuitaristsDto()
        {
            Guitars = new List<GetGuitarByIdDto>();
        }

        public int Id { get; set; }

        public string GuitaristName { get; set; }

        public string BandName { get; set; }

        public string Genre { get; set; }

        public IList<GetGuitarByIdDto> Guitars { get; set; }
        
    }
}
