﻿using System.Collections.Generic;
using demo.Application.Guitarists.Dto;

namespace demo.Application.Guitarists.Queries.GetGuitaristsWithPagination
{
    public class GuitaristsList
    {
        public ICollection<GuitaristListDto> Lists { get; set; }
    }
}
