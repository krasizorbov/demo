﻿
namespace demo.Application.Guitarists.Queries.GetGuitaristsWithPagination
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using demo.Application.Common.Interfaces;
    using demo.Domain.Entities;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetGuitaristsWithPaginationQuery : IRequest<List<GetGuitaristsDto>>
    {
        public int PageNumber { get; set; } = 0;
        public int? PageSize { get; set; } = 10;
        public int SkipPageCount => PageNumber * PageSize.GetValueOrDefault();
    }

    public class GetGuitaristsWithPaginationQueryHandler : IRequestHandler<GetGuitaristsWithPaginationQuery, List<GetGuitaristsDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetGuitaristsWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<GetGuitaristsDto>> Handle(GetGuitaristsWithPaginationQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Guitarist> guitarists = _context.Guitarists.AsQueryable<Guitarist>();

            if (request.SkipPageCount >= 0)
            {
                guitarists = guitarists.Skip(request.SkipPageCount).Take(request.PageSize.GetValueOrDefault());
            }
            return await guitarists
                .AsNoTracking()
                .ProjectTo<GetGuitaristsDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
