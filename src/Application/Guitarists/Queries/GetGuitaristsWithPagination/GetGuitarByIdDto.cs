﻿
namespace demo.Application.Guitarists.Queries.GetGuitaristsWithPagination
{
    using demo.Application.Common.Mappings;
    using demo.Domain.Entities;

    public class GetGuitarByIdDto : IMapFrom<Guitar>
    {
        public int Id { get; set; }

        public string Make { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }
    }
}
