﻿using System.Collections.Generic;
using demo.Application.Common.Mappings;
using demo.Application.Guitars.Dto;
using demo.Domain.Entities;

namespace demo.Application.Guitarists.Dto
{
    public class GuitaristListDto : IMapFrom<Guitarist>
    {
        public GuitaristListDto()
        {
            Guitars = new List<GetGuitarByIdDto>();
        }

        public int Id { get; set; }

        public string GuitaristName { get; set; }

        public string BandName { get; set; }

        public string Genre { get; set; }

        public IList<GetGuitarByIdDto> Guitars { get; set; }
    }
}
