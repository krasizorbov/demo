﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Exceptions;
using demo.Application.Common.Interfaces;
using demo.Domain.Entities;
using MediatR;

namespace demo.Application.Guitarists.Commands.UpdateGuitarist
{
    public class UpdateGuitaristCommand : IRequest
    {
        public int Id { get; set; }

        public string GuitaristName { get; set; }

        public string BandName { get; set; }

        public string Genre { get; set; }

    };


    public class UpdateGuitaristCommandHandler : IRequestHandler<UpdateGuitaristCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateGuitaristCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateGuitaristCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Guitarists.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Guitarist), request.Id);
            }

            entity.GuitaristName = request.GuitaristName;
            entity.BandName = request.BandName;
            entity.Genre = request.Genre;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
