﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace demo.Application.Guitarists.Commands.CreateGuitarist
{
    public class CreateGuitaristCommandValidator : AbstractValidator<CreateGuitaristCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateGuitaristCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.GuitaristName)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(20).WithMessage("Guitarist name must not exceed 20 characters.")
                .MustAsync(BeUniqueGuitaristName).WithMessage("The specified guitarist name already exists.");

            RuleFor(v => v.BandName)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(20).WithMessage("Band name must not exceed 20 characters.")
                .MustAsync(BeUniqueBandName).WithMessage("The specified band name already exists.");

            RuleFor(v => v.Genre)
                .NotEmpty().WithMessage("Genre is required.")
                .MaximumLength(20).WithMessage("Genre must not exceed 20 characters.");
        }

        public async Task<bool> BeUniqueGuitaristName(string name, CancellationToken cancellationToken)
        {
            return await _context.Guitarists
                .AllAsync(l => l.GuitaristName != name);
        }

        public async Task<bool> BeUniqueBandName(string name, CancellationToken cancellationToken)
        {
            return await _context.Guitarists
                .AllAsync(l => l.BandName != name);
        }
    }
}
