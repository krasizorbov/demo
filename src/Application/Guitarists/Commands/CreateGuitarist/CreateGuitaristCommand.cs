﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Interfaces;
using demo.Domain.Entities;
using MediatR;

namespace demo.Application.Guitarists.Commands.CreateGuitarist
{
    public class CreateGuitaristCommand : IRequest<int>
    {
        public string GuitaristName { get; set; }

        public string BandName { get; set; }

        public string Genre { get; set; }
    }

    public class CreateGuitaristCommandHandler : IRequestHandler<CreateGuitaristCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateGuitaristCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateGuitaristCommand request, CancellationToken cancellationToken)
        {
            
            var entity = new Guitarist
            {
                GuitaristName = request.GuitaristName,
                BandName = request.BandName,
                Genre = request.Genre
            };

            _context.Guitarists.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
