﻿using System.Threading;
using System.Threading.Tasks;
using demo.Application.Common.Exceptions;
using demo.Application.Common.Interfaces;
using demo.Domain.Entities;
using MediatR;

namespace demo.Application.Guitarists.Commands.DeleteGuitarist
{
    public class DeleteGuitaristCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeleteGuitaristCommandHandler : IRequestHandler<DeleteGuitaristCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteGuitaristCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteGuitaristCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Guitarists.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Guitarist), request.Id);
            }

            _context.Guitarists.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
