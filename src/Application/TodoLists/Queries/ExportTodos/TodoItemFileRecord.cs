﻿using demo.Application.Common.Mappings;
using demo.Domain.Entities;

namespace demo.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
