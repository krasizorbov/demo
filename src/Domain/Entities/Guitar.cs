﻿using demo.Domain.Common;

namespace demo.Domain.Entities
{
    public class Guitar : AuditableEntity
    { 
        public int Id { get; set; }

        public string Make { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public int GuitaristId { get; set; }

        public Guitarist Guitarist { get; set; }

    }
}
