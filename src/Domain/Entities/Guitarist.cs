﻿using System.Collections.Generic;

namespace demo.Domain.Entities
{
    public class Guitarist
    {
        public Guitarist()
        {
            Guitars = new List<Guitar>();
        }

        public int Id { get; set; }

        public string GuitaristName { get; set; }

        public string BandName { get; set; }

        public string Genre { get; set; }

        public IList<Guitar> Guitars { get; set; }

    }
}
